﻿using FpfxAssessment.DAL;
using FpfxFrontEndTest.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FpfxAssessment.Pages
{
	public class IndexModel : PageModel
	{
		private readonly IDatabase database;

		public string VolumeTraded { get; set; }
		public string BiggestLoss { get; set; }
		public string WinRate { get; set; }
		public string MostTradedAsset { get; set; }
		public string AverageTradeVolume { get; set; }
		public string AverageGain { get; set; }
		public string AverageRRR { get; set; }
		public int NumberOfTrades { get; set; }
		public string BiggestGain { get; set; }
		public string AverageLoss { get; set; }
		public int NumWinningTrades { get; set; }
        public int NumLosingTrades { get; set; }
		public string AverageTradeDuration { get; set; }

		public List<Trade> Trades { get; set; } = new List<Trade>();

		public IndexModel(IDatabase database)
		{
			this.database = database;
		}

		public void OnGet()
		{
			var accountId = 3;

			var account = database.GetAccounts().Where(x => x.AccountId == accountId).FirstOrDefault();

			var trades = database.GetTrades().Where(x => x.AccountId == account.AccountId).ToList();
			
			NumWinningTrades = trades.Where(x => x.Profit > 0).ToList().Count;
			
			NumLosingTrades = trades.Where(x => x.Profit < 0).ToList().Count;
		
			var mostTradedGrouped = trades.GroupBy(x => x.Symbol).Select(x => new
			{
				Symbol = x.Key,
				Count = x.Count()
			}).OrderBy(x => x.Count).FirstOrDefault();
			MostTradedAsset = mostTradedGrouped.Symbol;

			var numProfitableTrades = trades.Where(x => x.Profit > 0).Count();		
			
			AverageGain = numProfitableTrades > 0 ? (trades.Sum(x => x.Profit) / numProfitableTrades).ToString("C") : "0.00";

			double durationSeconds = 0;
			foreach (var trade in trades)
			{		
				var duration = trade.CloseTime.Subtract(trade.OpenTime);
				durationSeconds += duration.TotalSeconds;
			}		
			AverageTradeDuration = FormatDuration(TimeSpan.FromSeconds((durationSeconds / trades.Count)));

			AverageTradeVolume = (trades.Sum(x => x.Volume)).ToString("N2");

			BiggestGain = trades.Where(x => x.Profit > 0).OrderByDescending(x => x.Profit).Select(x => x.Profit).FirstOrDefault().ToString("C");
		
			BiggestLoss = trades.Where(x => x.Profit < 0).OrderByDescending(x => x.Profit).Select(x => x.Profit).FirstOrDefault().ToString("C");

			VolumeTraded = trades.Sum(x => x.Volume).ToString("N2");

			Trades = trades;


		}

		private static string FormatDuration(TimeSpan duration)
		{
			string result = string.Empty;
			if (duration.Days >= 1)
			{
				result += $"{duration.Days}d ";
			}
			if (duration.Hours >= 1)
			{
				result += $"{duration.Hours}h ";

			}
			if (duration.Minutes >= 1)
			{
				result += $"{duration.Minutes}m ";
			}
			if (duration.Seconds >= 1)
			{
				result += $"{duration.Seconds}s ";
			}

			return result;
		}
	}
}