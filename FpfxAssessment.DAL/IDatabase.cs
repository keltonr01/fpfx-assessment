﻿using FpfxFrontEndTest.DAL.Models;
using Microsoft.Extensions.Configuration;

namespace FpfxAssessment.DAL
{
	public interface IDatabase
	{
		IConfiguration Config { get; }

		List<AccountMetric> GetAccountMetrics();
		List<Account> GetAccounts();
		List<Customer> GetCustomers();
		List<Trade> GetTrades();
	}
}