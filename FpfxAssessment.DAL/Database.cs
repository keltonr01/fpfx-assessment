﻿using FpfxAssessment.DAL.Models;
using FpfxFrontEndTest.DAL.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Reflection;

namespace FpfxAssessment.DAL
{
	public class Database : IDatabase
	{
		private List<Trade> trades = new List<Trade>();
		private List<Account> accounts = new List<Account>();
		private List<Customer> customers = new List<Customer>();
		private List<AccountMetric> accountMetrics = new List<AccountMetric>();
		private List<Deposit> deposits = new List<Deposit>();

		public IConfiguration Config { get; private set; }

		public Database(IConfiguration configuration)
		{
			this.Config = configuration;
			LoadData();
		}

		private void LoadData()
		{
		
			using (var reader = new System.IO.StreamReader("../trades.json"))
			{
				var json = reader.ReadToEnd();
				trades = JsonConvert.DeserializeObject<List<Trade>>(json);		
			}

			using (var reader = new System.IO.StreamReader("../customers.json"))
			{
				var json = reader.ReadToEnd();
				customers = JsonConvert.DeserializeObject<List<Customer>>(json);
			}

			using (var reader = new System.IO.StreamReader("../accountmetrics.json"))
			{
				var json = reader.ReadToEnd();
				accountMetrics = JsonConvert.DeserializeObject<List<AccountMetric>>(json);
			}

			using (var reader = new System.IO.StreamReader("../accounts.json"))
			{
				var json = reader.ReadToEnd();
				accounts = JsonConvert.DeserializeObject<List<Account>>(json);
			}

			using (var reader = new System.IO.StreamReader("../deposits.json"))
			{
				var json = reader.ReadToEnd();
				deposits = JsonConvert.DeserializeObject<List<Deposit>>(json);
			}
		}

		public List<Trade> GetTrades() { return trades;}
		public List<Customer> GetCustomers() { return customers; }
		public List<Account> GetAccounts() { return accounts; }
		public List<AccountMetric> GetAccountMetrics() { return accountMetrics; }

		


	}
}
