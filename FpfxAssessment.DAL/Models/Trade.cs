﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpfxFrontEndTest.DAL.Models
{
    public class Trade
    {
        public int TradeId { get; set; }
        public int AccountId { get; set; }
        public string Symbol { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal ClosePrice { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime CloseTime { get; set; }
        public decimal Profit { get; set; }
        public string Side { get; set; }
        public decimal Lots { get; set; }
        public decimal Volume { get; set; }

	}
}
