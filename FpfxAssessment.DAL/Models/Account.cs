﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpfxFrontEndTest.DAL.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public int Login { get; set; }
        public string Plan { get; set; }
        public decimal MaxLossLimitPct { get; set; }
        public decimal DailyLossLimitPct { get; set; }


    }
}
