﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpfxFrontEndTest.DAL.Models
{
    public class AccountMetric
    {
        public int AccountId { get; set; }
        public int TradeDate { get; set; }
        public decimal Balance { get; set; }
        public decimal Equity { get; set; }
        public DateTime Date { get; set; }
		public decimal DailyLossLimit { get; set; }
        public decimal MaxLossLimit { get; set; }
        public decimal TodayProfit { get; set; }

    }
}
