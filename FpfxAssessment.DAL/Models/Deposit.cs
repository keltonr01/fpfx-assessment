﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpfxAssessment.DAL.Models
{
	public class Deposit
	{
		public int DepositId { get; set; }
        public int TradeDate { get; set; }
		public decimal Amount { get; set; }
        public string Currency { get; set; }
		public DateTime Timestamp { get; set; }
	}
}
